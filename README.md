## About
Tenhou flash-client protocol debugging proxy

Add to your `hosts` file
```
127.0.0.1	b.mjv.jp
```

```
go run ./main.go
```

Login into flash client.
Protocol logs will appear in workdir.


